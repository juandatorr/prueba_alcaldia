<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Auth::routes();


Route::middleware('jwt.auth')->group(function () {
    Route::get('users', function () {
        return auth()->user();
    });

    //Rutas de las tablas de configuraciones
    Route::resource('tabla-configuracion', 'Configuracion\TablaConfiguracionController');
    Route::resource('registro-configuracion', 'Configuracion\RegistroConfiguracionController');

    //Rutas de las tablas de ubicación
    Route::resource('departamento', 'Ubicacion\DepartamentoController');
    Route::resource('municipio', 'Ubicacion\MunicipioController');

    //Rutas de las tablas de Institución
    Route::resource('institucion', 'Institucion\InstitucionController');
    Route::resource('sede', 'Institucion\SedeController');

    //Rutas de las tablas de Grado
    Route::resource('grado', 'Grado\GradoController');
    Route::resource('curso', 'Grado\CursoController');

    //Rutas de las tablas de Estudiante
    Route::resource('estudiante', 'Estudiante\EstudianteController');
    Route::resource('estudiante-matricula', 'Estudiante\EstudianteMatriculaController');

});