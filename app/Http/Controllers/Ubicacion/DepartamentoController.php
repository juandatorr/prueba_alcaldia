<?php

namespace App\Http\Controllers\Ubicacion;

use App\Modelos\Ubicacion\Departamento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class DepartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $departamento = Departamento::all();
        if ($departamento->isEmpty()){
            return Response::json(['No existe datos' => true]);
        }else{
            return Response::json($departamento, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'codigo_dane' => 'required|numeric|unique:departamento'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $departamento = new Departamento();
            $departamento->nombre = $request->nombre;
            $departamento->codigo_dane = $request->codigo_dane;

            if($departamento->save() == true){
                return Response::json(['creado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $departamento = Departamento::find($id);
            if (isset($departamento)) {
                return Response::json($departamento, 200);
            } else {
                return Response::json(['No existe' => true], 200);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'codigo_dane' => 'required|numeric'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $departamento = Departamento::find($id);
            $departamento->nombre = $request->nombre;
            $departamento->codigo_dane = $request->codigo_dane;

            if($departamento->update() == true){
                return Response::json(['actualizado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $departamento = Departamento::find($id);
            if(isset($departamento)){
                if ($departamento->delete() == true) {
                    return Response::json(['eliminado' => true], 200);
                } else {
                    return Response::json(['error' => false], 400);
                }
            }else{
                return Response::json(['No existe' => true]);
            }

        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }

    }
}
