<?php

namespace App\Http\Controllers\Institucion;

use App\Modelos\Institucion\Institucion;
use App\Transformers\Institucion\InstitucionTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class InstitucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $instituciones = Institucion::all()->transformWith(new InstitucionTransformer())->toArray();
        if (empty($instituciones)){
            return Response::json(['No existe datos' => true]);
        }else{
            return Response::json($instituciones, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'nit' => 'required|numeric|unique:institucion',
                'nombre' => 'required',
                'departamento_id' => 'required|exists:departamento,id',
                'municipio_id' => 'required|exists:municipio,id',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $institucion = new Institucion();
            $institucion->nit = $request->nit;
            $institucion->nombre = $request->nombre;
            $institucion->telefono = $request->telefono;
            $institucion->direccion = $request->direccion;
            $institucion->departamento_id = $request->departamento_id;
            $institucion->municipio_id = $request->municipio_id;

            if($institucion->save() == true){
                return Response::json(['creado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $institucion = Institucion::find($id);
            if (isset($institucion)) {
                return Response::json($institucion, 200);
            } else {
                return Response::json(['No existe' => true], 200);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'departamento_id' => 'required|exists:departamento,id',
                'municipio_id' => 'required|exists:municipio,id',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $institucion = Institucion::find($id);
            $institucion->nombre = $request->nombre;
            $institucion->telefono = $request->telefono;
            $institucion->direccion = $request->direccion;
            $institucion->departamento_id = $request->departamento_id;
            $institucion->municipio_id = $request->municipio_id;

            if($institucion->update() == true){
                return Response::json(['actualizado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $institucion = Institucion::find($id);
            if(isset($institucion)){
                if ($institucion->delete() == true) {
                    return Response::json(['eliminado' => true], 200);
                } else {
                    return Response::json(['error' => false], 400);
                }
            }else{
                return Response::json(['No existe' => true]);
            }

        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }
}
