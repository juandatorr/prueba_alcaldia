<?php

namespace App\Http\Controllers\Institucion;

use App\Modelos\Institucion\Sede;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class SedeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sedes = Sede::all();
        if ($sedes->isEmpty()){
            return Response::json(['No existe datos' => true]);
        }else{
            return Response::json($sedes, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'departamento_id' => 'required|exists:departamento,id',
                'municipio_id' => 'required|exists:municipio,id',
                'institucion_id' => 'required|exists:institucion,id',

            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $sede = new Sede();
            $sede->nombre = $request->nombre;
            $sede->telefono = $request->telefono;
            $sede->direccion = $request->direccion;
            $sede->departamento_id = $request->departamento_id;
            $sede->municipio_id = $request->municipio_id;
            $sede->institucion_id = $request->institucion_id;

            if($sede->save() == true){
                return Response::json(['creado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $sede = Sede::find($id);
            if (isset($sede)) {
                return Response::json($sede, 200);
            } else {
                return Response::json(['No existe' => true], 200);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'departamento_id' => 'required|exists:departamento,id',
                'municipio_id' => 'required|exists:municipio,id',
                'institucion_id' => 'required|exists:institucion,id',

            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $sede = Sede::find($id);
            $sede->nombre = $request->nombre;
            $sede->telefono = $request->telefono;
            $sede->direccion = $request->direccion;
            $sede->municipio_id = $request->municipio_id;
            $sede->institucion_id = $request->institucion_id;

            if($sede->save() == true){
                return Response::json(['actualizado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $sede = Sede::find($id);
            if(isset($sede)){
                if ($sede->delete() == true) {
                    return Response::json(['eliminado' => true], 200);
                } else {
                    return Response::json(['error' => false], 400);
                }
            }else{
                return Response::json(['No existe' => true]);
            }

        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }
}
