<?php

namespace App\Http\Controllers\Estudiante;

use App\Modelos\Estudiante\Estudiante;
use App\Transformers\Estudiante\EstudianteTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Response;
use Validator;


class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $estudiante = Estudiante::all()->transformWith(new EstudianteTransformer())->toArray();
        if (empty($estudiante)){
            return Response::json(['No existe datos' => true]);
        }else{
            return Response::json($estudiante, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'tipo_documento' => 'required|exists:registro_configuracion,id',
                'numero_documento' => 'required|numeric',
                'fecha_nacimiento' => 'required|date',
                'genero_id' => 'required|exists:registro_configuracion,id',
                'primer_nombre' => 'required',
                'primer_apellido' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $estudiante = new Estudiante();
            $estudiante->tipo_documento = $request->tipo_documento;
            $estudiante->numero_documento = $request->numero_documento;
            $estudiante->departamento_expedicion_documento	 = $request->departamento_expedicion_documento;
            $estudiante->municipio_expedicion_documento	 = $request->municipio_expedicion_documento;
            $estudiante->fecha_nacimiento = $request->fecha_nacimiento;
            $estudiante->genero_id = $request->genero_id;
            $estudiante->primer_nombre = $request->primer_nombre;
            $estudiante->segundo_nombre = $request->segundo_nombre;
            $estudiante->primer_apellido = $request->primer_apellido;
            $estudiante->segundo_apellido = $request->segundo_apellido;
            $estudiante->direccion_residencia = $request->direccion_residencia;
            $estudiante->telefono_residencia = $request->telefono_residencia;
            $estudiante->estrato_id = $request->estrato_id;
            $estudiante->user_id = Auth::user()->id;

            if($estudiante->save() == true){
                return Response::json(['creado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $estudiante = Estudiante::find($id);
            if (isset($estudiante)) {
                return Response::json($estudiante, 200);
            } else {
                return Response::json(['No existe' => true], 200);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'tipo_documento' => 'required|exists:registro_configuracion,id',
                'numero_documento' => 'required|numeric',
                'fecha_nacimiento' => 'required|date',
                'genero_id' => 'required|exists:registro_configuracion,id',
                'primer_nombre' => 'required',
                'primer_apellido' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $estudiante = Estudiante::find($id);
            $estudiante->tipo_documento = $request->tipo_documento;
            $estudiante->numero_documento = $request->numero_documento;
            $estudiante->departamento_expedicion_documento	 = $request->departamento_expedicion_documento;
            $estudiante->municipio_expedicion_documento	 = $request->municipio_expedicion_documento;
            $estudiante->fecha_nacimiento = $request->fecha_nacimiento;
            $estudiante->genero_id = $request->genero_id;
            $estudiante->primer_nombre = $request->primer_nombre;
            $estudiante->segundo_nombre = $request->segundo_nombre;
            $estudiante->direccion_residencia = $request->direccion_residencia;
            $estudiante->telefono_residencia = $request->telefono_residencia;
            $estudiante->estrato_id = $request->estrato_id;

            if($estudiante->update() == true){
                return Response::json(['actualizado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $estudiante = Estudiante::find($id);
            if(isset($estudiante)){
                if ($estudiante->delete() == true) {
                    return Response::json(['eliminado' => true], 200);
                } else {
                    return Response::json(['error' => false], 400);
                }
            }else{
                return Response::json(['No existe' => true]);
            }

        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }
}
