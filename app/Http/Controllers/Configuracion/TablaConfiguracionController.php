<?php

namespace App\Http\Controllers\Configuracion;

use App\Modelos\Configuracion\TablaConfiguracion;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class TablaConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tablas = TablaConfiguracion::all();
        if ($tablas->isEmpty()){
            return Response::json(['No existe datos' => true]);
        }else{
            return Response::json($tablas, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {

            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $tabla = new TablaConfiguracion();
            $tabla->nombre = $request->nombre;

            if($tabla->save() == true){
                return Response::json(['creado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $tabla = TablaConfiguracion::find($id);
            if (isset($tabla)) {
                return Response::json($tabla, 200);
            } else {
                return Response::json(['No existe' => true], 200);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $tabla = TablaConfiguracion::find($id);
            $tabla->nombre = $request->nombre;

            if($tabla->update() == true){
                return Response::json(['actualizado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $tabla = TablaConfiguracion::find($id);
            if(isset($tabla)){
                if ($tabla->delete() == true) {
                    return Response::json(['eliminado' => true], 200);
                } else {
                    return Response::json(['error' => false], 400);
                }
            }else{
                return Response::json(['No existe' => true]);
            }

        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }

    }
}
