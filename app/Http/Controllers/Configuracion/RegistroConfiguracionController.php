<?php

namespace App\Http\Controllers\Configuracion;

use App\Modelos\Configuracion\RegistroConfiguracion;
use App\Transformers\Configuracion\RegistroConfiguracionTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Validator;

class RegistroConfiguracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $registro = RegistroConfiguracion::all()->transformWith(new RegistroConfiguracionTransformer())->toArray();
        if (empty($registro)){
            return Response::json(['No existe datos' => true]);
        }else{
            return Response::json($registro, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {

            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
                'tabla_id' => 'required|exists:tabla_configuracion,id'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $registro = new RegistroConfiguracion();
            $registro->nombre = $request->nombre;
            $registro->tabla_id = $request->tabla_id;

            if($registro->save() == true){
                return Response::json(['creado' => true], 200);
            }else{
                return Response::json(['error' => false], 400);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $registro = RegistroConfiguracion::find($id);
            if (isset($registro)) {
                return Response::json($registro, 200);
            } else {
                return Response::json(['No existe' => true], 200);
            }
        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {

            $validator = Validator::make($request->all(), [
                'nombre' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $registro = RegistroConfiguracion::find($id);
            if (isset($registro)){
                $registro->nombre = $request->nombre;
                if($registro->update() == true){
                    return Response::json(['actualizado' => true], 200);
                }else{
                    return Response::json(['error' => false], 400);
                }
            }else{
                return Response::json(['No existe' => true], 200);
            }

        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $registro = RegistroConfiguracion::find($id);
            if(isset($registro)){
                if ($registro->delete() == true) {
                    return Response::json(['eliminado' => true], 200);
                } else {
                    return Response::json(['error' => false], 400);
                }
            }else{
                return Response::json(['No existe' => true]);
            }

        }catch (Exception $e) {
            // Si algo sale mal devolvemos un error.
            return Response::json(['error' => false], 500);
        }
    }
}
