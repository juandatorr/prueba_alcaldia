<?php

namespace App\Transformers\Configuracion;

use League\Fractal\TransformerAbstract;

class RegistroConfiguracionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($registro)
    {
        return [
            //
            'id' => $registro->id,
            'nombre' => $registro->nombre,
        ];
    }
}
