<?php

namespace App\Transformers\Configuracion;

use League\Fractal\TransformerAbstract;

class TablaConfiguracionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($tabla)
    {
        return [
            //
            'id' => $tabla->id,
            'nombre' => $tabla->nombre
        ];
    }
}
