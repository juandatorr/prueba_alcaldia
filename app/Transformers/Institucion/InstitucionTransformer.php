<?php

namespace App\Transformers\Institucion;

use League\Fractal\TransformerAbstract;

class InstitucionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($institucion)
    {
        return [
            //
            'nit' => $institucion->nit,
            'nombre' => $institucion->nombre,
            'telefono' => $institucion->telefono,
            'direccion' => $institucion->direccion,
            'departamento' => $institucion->departamento_id,
            'muncipio' => $institucion->municipio_id,
        ];
    }
}
