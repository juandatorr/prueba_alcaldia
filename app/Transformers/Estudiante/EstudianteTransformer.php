<?php

namespace App\Transformers\Estudiante;

use League\Fractal\TransformerAbstract;

class EstudianteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($estudiante)
    {
        return [
            //
            'tipo' => $estudiante->tipo_documento,
            'numero' => $estudiante->numero_documento,
            'departamento' => $estudiante->departamento_expedicion_documento,
            'municipio' => $estudiante->municipio_expedicion_documento,
            'nacimiento' => $estudiante->fecha_nacimiento,
            'genero' => $estudiante->genero_id,
            'primer_nombre' => $estudiante->primer_nombre,
            'segundo_nombre' => $estudiante->segundo_nombre,
            'usuario' => $estudiante->user_id,
        ];
    }
}
