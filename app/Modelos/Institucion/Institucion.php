<?php

namespace App\Modelos\Institucion;

use App\Transformers\Institucion\InstitucionTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Institucion extends Model
{
    //
    use SoftDeletes;
    protected $table = 'institucion';
    public $transformers = InstitucionTransformer::class;

}
