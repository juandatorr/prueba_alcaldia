<?php

namespace App\Modelos\Institucion;

use App\Transformers\Institucion\SedeTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sede extends Model
{
    //
    use SoftDeletes;
    protected $table = 'sede';
    public $transformers = SedeTransformer::class;

}
