<?php

namespace App\Modelos\Ubicacion;

use App\Transformers\Ubicacion\DepartamentoTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Departamento extends Model
{
    //
    use SoftDeletes;
    protected $table = 'departamento';
    public $transformers = DepartamentoTransformer::class;


}
