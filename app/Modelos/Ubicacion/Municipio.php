<?php

namespace App\Modelos\Ubicacion;

use App\Transformers\Ubicacion\MunicipioTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
    //
    use SoftDeletes;
    protected $table = 'municipio';
    public $transformers = MunicipioTransformer::class;

}
