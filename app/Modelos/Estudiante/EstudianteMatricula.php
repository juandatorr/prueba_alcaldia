<?php

namespace App\Modelos\Estudiante;

use App\Transformers\Estudiante\EstudianteMatriculaTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstudianteMatricula extends Model
{
    //
    use SoftDeletes;
    protected $table = 'estudiante_matricula';
    public $transformers = EstudianteMatriculaTransformer::class;

}
