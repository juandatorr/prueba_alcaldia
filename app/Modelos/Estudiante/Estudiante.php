<?php

namespace App\Modelos\Estudiante;

use App\Transformers\Estudiante\EstudianteMatriculaTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estudiante extends Model
{
    //
    use SoftDeletes;
    protected $table = 'estudiante';
    public $transformers = EstudianteMatriculaTransformer::class;

}
