<?php

namespace App\Modelos\Configuracion;

use App\Transformers\Configuracion\RegistroConfiguracionTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistroConfiguracion extends Model
{
    //
    use SoftDeletes;
    protected $table = 'registro_configuracion';
    public $transformers = RegistroConfiguracionTransformer::class;

}
