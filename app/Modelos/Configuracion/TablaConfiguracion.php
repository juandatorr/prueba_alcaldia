<?php

namespace App\Modelos\Configuracion;

use App\Transformers\Configuracion\TablaConfiguracionTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TablaConfiguracion extends Model
{
    //
    use SoftDeletes;
    protected $table = 'tabla_configuracion';
    public $transformers = TablaConfiguracionTransformer::class;

}
