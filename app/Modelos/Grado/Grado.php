<?php

namespace App\Modelos\Grado;

use App\Transformers\Grado\GradoTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grado extends Model
{
    //
    use SoftDeletes;
    protected $table = 'grado';
    public $transformers = GradoTransformer::class;

}

