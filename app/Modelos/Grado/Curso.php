<?php

namespace App\Modelos\Grado;

use App\Transformers\Grado\CursoTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curso extends Model
{
    //
    use SoftDeletes;
    protected $table = 'curso';
    public $transformers = CursoTransformer::class;

}
