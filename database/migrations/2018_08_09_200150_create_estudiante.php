<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiante extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiante', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->unsignedInteger('tipo_documento');
            $table->integer('numero_documento')->unique();
            $table->unsignedInteger('departamento_expedicion_documento')->nullable();
            $table->unsignedInteger('municipio_expedicion_documento')->nullable();
            $table->date('fecha_nacimiento');
            $table->unsignedInteger('genero_id');
            $table->string('primer_nombre');
            $table->string('segundo_nombre')->nullable();
            $table->string('primer_apellido');
            $table->string('segundo_apellido')->nullable();
            $table->string('direccion_residencia')->nullable();
            $table->string('telefono_residencia')->nullable();
            $table->unsignedInteger('estrato_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('tipo_documento')->references('id')->on('registro_configuracion');
            $table->foreign('departamento_expedicion_documento')->references('id')->on('departamento');
            $table->foreign('municipio_expedicion_documento')->references('id')->on('municipio');
            $table->foreign('estrato_id')->references('id')->on('registro_configuracion');
            $table->foreign('genero_id')->references('id')->on('registro_configuracion');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estudiante', function (Blueprint $table) {
            //
            $table->dropForeign(['tipo_documento', 'departamento_expedicion_documento', 'municipio_expedicion_documento', 'estrato_id', 'genero_id', 'user_id']);
        });

        Schema::dropIfExists('estudiante');
    }
}
