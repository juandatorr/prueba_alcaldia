<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSede extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sede', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('nombre');
            $table->string('telefono')->nullable();
            $table->string('direccion')->nullable();
            $table->unsignedInteger('departamento_id');
            $table->unsignedInteger('municipio_id');
            $table->unsignedInteger('institucion_id');
            $table->integer('estado')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('departamento_id')->references('id')->on('departamento');
            $table->foreign('municipio_id')->references('id')->on('municipio');
            $table->foreign('institucion_id')->references('id')->on('institucion');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sede', function (Blueprint $table) {
            //
            $table->dropForeign(['departamento_id', 'municipio_id', 'institucion_id']);
        });
        Schema::dropIfExists('sede');
    }
}
