<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatriculaEstudiante extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matricula_estudiante', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->date('fecha_matricula');
            $table->unsignedInteger('jornada_id')->nullable();
            $table->unsignedInteger('caracter_id')->nullable();
            $table->unsignedInteger('especialidad_id')->nullable();

            $table->unsignedInteger('grado_id');
            $table->unsignedInteger('curso_id');
            $table->unsignedInteger('user_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('jornada_id')->references('id')->on('registro_configuracion');
            $table->foreign('caracter_id')->references('id')->on('registro_configuracion');
            $table->foreign('especialidad_id')->references('id')->on('registro_configuracion');
            $table->foreign('grado_id')->references('id')->on('grado');
            $table->foreign('curso_id')->references('id')->on('curso');
            $table->foreign('user_id')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matricula_estudiante', function (Blueprint $table) {
            //
            $table->dropForeign(['jornada_id', 'caracter_id', 'especialidad_id', 'grado_id', 'curso_id', 'user_id']);
        });

        Schema::dropIfExists('matricula_estudiante');

    }
}
