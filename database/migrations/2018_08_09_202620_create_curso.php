<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('nombre');
            $table->unsignedInteger('grado_id');
            $table->integer('estado')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('grado_id')->references('id')->on('grado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('curso', function (Blueprint $table) {
            //
        });
    }
}
