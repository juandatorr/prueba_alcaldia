<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroConfiguracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_configuracion', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->unsignedInteger('tabla_id');
            $table->string('nombre');
            $table->integer('estado')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('tabla_id')->references('id')->on('tabla_configuracion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registro_configuracion', function (Blueprint $table) {
            //
            $table->dropForeign('tabla_id');
        });
        Schema::dropIfExists('registro_configuracion');
    }
}
